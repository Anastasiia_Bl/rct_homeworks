import { basketTypes } from "./types";
import { getProductsFromLS } from "../../utils";

const lsBasket = getProductsFromLS("arrBasket");

const initialState = {
    arrBasket: lsBasket,
  };

  export const basketReducer = (state = initialState, action) => {
    switch (action.type) {
      case basketTypes.ADD_TO_BASKET:
        return {
            ...state,
            arrBasket: action.payload,
        }

      case basketTypes.DELETE_FROM_BASKET:
        return {
            ...state,
            arrBasket: action.payload,
        }

        case basketTypes.CLEAN_BASKET:
          return {
              ...state,
              arrBasket: [],
          }

      default:
        return state;
    }
  };
  

