import { modalTypes } from './type';

const initialState = {
    modalIsOpen: false,
    activeModal: null,
    selectedProduct: null,
}


export const modalReducer = (state = initialState, action) => {
  console.log(state)
  console.log(action)
    switch (action.type) {
      case modalTypes.OPEN_MODAL:
        return {
          ...state,
          modalIsOpen: true,
          activeModal: action.payload.modal,
          selectedProduct: action.payload.product,
        };
      case modalTypes.CLOSE_MODAL:
        return {
          ...state,
          modalIsOpen: false,
          activeModal: null,
        };
      default:
        return state;
    }
  };
