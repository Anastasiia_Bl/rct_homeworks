export const modalConfigs = [
    {
        id: 'deleteModal',
        header: "Do you want to delete this item from Basket?",
        closeButton: true,
        text: "Once you delete this file, it won`t be in your Basket. Are you sure you want to delete it?",
    },

    {
        id: 'sendModal',
        header: "Do you want to send this file?",
        closeButton: true,
        text: "Are you sure you want to send it? Make sure that everything is correct.",
    },

    {
        id: 'basketModal',
        header: "Do you want to add to basket this item?",
        closeButton: true,
        text: "This item will be in your basket and you can continue your shopping.",
    }
]

