// import ProductCard from "../components/productCard/productCard";
import { useSelector } from "react-redux";
import ProductList from "../components/productList/productList";
import Form from "../components/form/form";

export const BasketPage = (props) => {
  const arrBasket = useSelector((state) => state.basket.arrBasket);

  return (
    <>
      <ProductList arrProducts={arrBasket} {...props} />
      {arrBasket.length !== 0 && (<Form arrBasket={arrBasket}/>)}
    </>
  );
  
};





// export const BasketPage = (props) => {
//   const { openModal, modalId, btnCardText, deleteButton} = props;
//   const arrBasket = useSelector((state) => state.basket.arrBasket);

//   const productCards = arrBasket.map((item) => (
//     <ProductCard
//       key={item.id}
//       product={item}
//       openModal={openModal}
//       modalId={modalId}
//       btnCardText={btnCardText}
//       deleteButton={deleteButton}
//     />
//   ));

//   return <ul className="products__list">{productCards}</ul>;
// };

