
import "./backet.scss";
import PropTypes from "prop-types";
import bascketlogo from './img/add-icon.svg';
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";


const Basket = (props) => {

  const arrBasket = useSelector((state) => state.basket.arrBasket);

      return (
      <NavLink to="/basket" className="header__basket">
        <img src={bascketlogo} alt="img" />
        {arrBasket.length}
      </NavLink>
    );

  }

export default Basket;

Basket.propTypes = {
  arrBasket: PropTypes.array,
};
