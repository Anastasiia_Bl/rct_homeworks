import {useFormik} from "formik";
import { cleanBasketAction } from "../../redux/basket/action";
import { useDispatch } from "react-redux";
import './form.scss';
import * as Yup from 'yup';
import { PatternFormat } from 'react-number-format';

const validationSchema = Yup.object({
    firstName: Yup.string().matches(/^[a-zA-Z-]+$/, "Only letters").min(2, 'Minimum 2 letters required').max(20, 'Only 20 letters allowed').required('This field is required'),
    lastName: Yup.string().matches(/^[a-zA-Z-]+$/, "Only letters").min(2, 'Minimum 2 letters required').max(20, 'Only 20 letters allowed').required('This field is required'),
    age: Yup.number().min(18).max(100).required('This field is required'),
    address: Yup.string().min(10, 'Too short delivery address').max(100).required('This field is required'),
    mobileNumber: Yup.string().matches(/^\(\d{3}\)\d{3}-\d{2}-\d{2}$/, 'Invalid phone number').required('This field is required'),
})

export default function Form (props) {
    const dispatch = useDispatch();


    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            mobileNumber: '',
        },

        validationSchema,

        onSubmit: (values)=> {
            console.log(values);
            console.log(props.arrBasket);
            dispatch(cleanBasketAction());
            formik.resetForm(); // Сбросить значения формы после отправки
        }
    });

  return (
    <>
    <h2>Complete your order</h2>
    <form onSubmit={formik.handleSubmit} className="basketForm">
        <label htmlFor="firstName">First Name</label>
        <input type="text" id="firstName" name="firstName" placeholder="first name" value={formik.values.firstName} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
        {formik.touched.firstName && formik.errors.firstName ? <span>{formik.errors.firstName}</span> : null}

        <label htmlFor="lastName">Last Name</label>
        <input type="text" id="lastName" name="lastName" placeholder="last name" value={formik.values.lastName} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
        {formik.touched.lastName && formik.errors.lastName ? <span>{formik.errors.lastName}</span> : null}

        <label htmlFor="age">Age</label>
        <input type="number" id="age" name="age" placeholder="age" value={formik.values.age} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
        {formik.touched.age && formik.errors.age ? <span>{formik.errors.age}</span> : null}

        <label htmlFor="address">Address</label>
        <input type="text" id="address" name="address" placeholder="address" value={formik.values.address} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
        {formik.touched.address && formik.errors.address ? <span>{formik.errors.address}</span> : null}

        <label htmlFor="mobileNumber">Mobile Number</label>
        {/* <input type="number" id="mobileNumber" name="mobileNumber" placeholder="mobile number" value={formik.values.mobileNumber} onChange={formik.handleChange}/> */}
        <PatternFormat
          format="(###)###-##-##"
          mask="_"
          id="mobileNumber"
          name="mobileNumber"
          placeholder="Mobile number"
          value={formik.values.mobileNumber}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {formik.touched.mobileNumber && formik.errors.mobileNumber ? <span>{formik.errors.mobileNumber}</span> : null}

        <button type="submit" className="basketForm_button">Checkout</button>
    </form>
    </>
  );
}
