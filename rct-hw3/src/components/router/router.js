import { Route, Routes } from "react-router-dom";
import { FavouritesPage } from '../../pages/favouritesPage';
import { BasketPage } from '../../pages/basketPage';
import ProductList from "../productList/productList";



export function Router(props) {

    const {arrBasket, openModal, addToFavourites, arrFavourites, arrProducts} = props;

    return (
        <Routes>
        <Route path="/" element={<ProductList arrProducts={arrProducts} openModal={openModal} modalId='basketModal' addToFavourites={addToFavourites} btnCardText='Add to cart'/>}/>
        <Route path="/favourites" element={<FavouritesPage arrFavourites={arrFavourites} openModal={openModal} modalId='basketModal' addToFavourites={addToFavourites} btnCardText='Add to cart'/>}/>
        <Route path="/basket" element={<BasketPage arrBasket={arrBasket} openModal={openModal} modalId='deleteModal' addToFavourites={addToFavourites} btnCardText='Delete' deleteButton={true} />}/>
      </Routes>
    )
}