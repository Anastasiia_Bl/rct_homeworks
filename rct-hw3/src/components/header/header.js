
import './header.scss';
import Basket from "../basket/basket";
import Favourites from "../favourites/favourites";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";


const Header = (props) => {

  return (
    <header className="header">
      <NavLink to="/" className="header__title">Online coffee shop</NavLink>
      <div className="header__icons">
        <Basket arrBasket={props.arrBasket} />
        <Favourites arrFavourites={props.arrFavourites} />
      </div>
    </header>
);

}

Header.propTypes = {
  arrBasket: PropTypes.array,
  arrFavourites: PropTypes.array,
};

export default Header;



// class Header extends Component {

//     render() {
//         return (
//             <header className="header">
//               <h2 className="header__title">Online coffee shop</h2>
//               <div className="header__icons">
//                 <Basket arrBasket={this.props.arrBasket} />
//                 <Favourites arrFavourites={this.props.arrFavourites} />
//               </div>
//             </header>
//         );
//       }
// }

