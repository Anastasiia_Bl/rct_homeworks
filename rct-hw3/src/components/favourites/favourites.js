
import "./favourites.scss";
import PropTypes from "prop-types";
import star from './img/star.svg'
import { NavLink } from "react-router-dom";


const Favourites = (props) => {

  return (
    <NavLink to="/favourites" className="header__favourites">
    <img src={star} alt="img" />
    {props.arrFavourites.length}
  </NavLink>
  );

}

Favourites.propTypes = {
  arrFavourites: PropTypes.array,
};


export default Favourites;

// class Favourites extends Component {

//   render() {
//     return (
//       <a className="header__favourites" href="/#">
//         <img src={star} alt="img" />
//         {this.props.arrFavourites.length}
//       </a>
//     );
//   }
// }




