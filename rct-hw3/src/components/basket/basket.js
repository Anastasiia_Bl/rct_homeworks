
import "./backet.scss";
import PropTypes from "prop-types";
import bascketlogo from './img/add-icon.svg';
import { NavLink } from "react-router-dom";

// class Basket extends Component {

//   render() {
//     return (
//       <a href="/#" className="header__basket">
//         <img src={bascketlogo} alt="img" />
//         {this.props.arrBasket.length}
//       </a>
//     );
//   }
// }


const Basket = (props) => {

      return (
      <NavLink to="/basket" className="header__basket">
        <img src={bascketlogo} alt="img" />
        {props.arrBasket.length}
      </NavLink>
    );

  }

export default Basket;

Basket.propTypes = {
  arrBasket: PropTypes.array,
};
