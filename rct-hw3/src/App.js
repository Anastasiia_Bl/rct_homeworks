

import React, { useState, useEffect } from 'react';
import './App.css';
import Button from './components/button/button.js';
import Modal from './components/modal/modal.js';
import Header from "./components/header/header";
import { modalConfigs } from "./arrModalConfigues";
import { Router } from './components/router/router';



const getProductsFromLS = (key) => {
  const lsProducts = localStorage.getItem(key);
  if (!lsProducts) return [];
  try {
    const value = JSON.parse(lsProducts);
    return value;
  } catch (e) {
    return [];
  }
};

const App = () => {
  const lsFavorites = getProductsFromLS("arrFavourites");
  const lsBasket = getProductsFromLS("arrBasket");
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [activeModal, setActiveModal] = useState(null);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [arrProducts, setArrProducts] = useState([]);
  const [arrFavourites, setArrFavourites] = useState(lsFavorites);
  const [arrBasket, setArrBasket] = useState([]);

  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) => {
        setArrProducts(data);
      });

    setArrBasket(lsBasket);
  
  }, []);
  

  const openModal = (modalId, product) => {
    const modal = modalConfigs.find(modal => modal.id === modalId);
    console.log(modal)
    setModalIsOpen(true);
    setActiveModal(modal);
    setSelectedProduct(product);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setActiveModal(null);
  };

  // const addToBasket = () => {
  //   if (selectedProduct) {
  //     setArrBasket([...arrBasket, selectedProduct]);
  //     setSelectedProduct(null);
  //   }
  //   closeModal();
  // };
  // const addToBasket = () => {
  //   if (selectedProduct) {
  //     setArrBasket((prevArrBasket) => [
  //       ...prevArrBasket,
  //       selectedProduct,
  //     ]);
  //     setSelectedProduct(null);
  //   }
  //   closeModal();
  // };

  const addToBasket = () => {
    if (selectedProduct) {
      if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
        closeModal();
        return arrBasket; // повертаю масив продуктів без змін
      } else {
        setArrBasket([...arrBasket, selectedProduct]);
        localStorage.setItem("arrBasket", JSON.stringify([...arrBasket, selectedProduct]));
      }
      setSelectedProduct(null);
    }
    closeModal();
    return arrBasket;
  };

  const deleteFromBasket = () => {
  if (selectedProduct) {
    if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
      const newBasket = arrBasket.filter((product) => JSON.stringify(product) !== JSON.stringify(selectedProduct));
      setArrBasket(newBasket);
      localStorage.setItem("arrBasket", JSON.stringify(newBasket));
    } 
    setSelectedProduct(null);
  }
  closeModal();
  return arrBasket;
};

  const addToFavourites = (favouriteProduct) => {
    const index = arrFavourites.findIndex((product) => {
      return JSON.stringify(product) === JSON.stringify(favouriteProduct);
    });

    if (index === -1) {
      setArrFavourites([...arrFavourites, favouriteProduct]);
      console.log("add");
    } else {
      const newFavourites = [
        ...arrFavourites.slice(0, index),
        ...arrFavourites.slice(index + 1),
      ];
      setArrFavourites(newFavourites);
    }
  };

   // оновлення local storage
   useEffect(() => {
    console.log(arrFavourites);
    localStorage.setItem("arrFavourites", JSON.stringify(arrFavourites));
    localStorage.setItem("arrBasket", JSON.stringify(arrBasket));
  }, [arrFavourites, arrBasket]);


  return (
    <div className="App">
      <Header arrBasket={arrBasket} arrFavourites={arrFavourites}/>

      <Router arrProducts={arrProducts} openModal={openModal} arrFavourites={arrFavourites} arrBasket={arrBasket} addToFavourites={addToFavourites}/>
      
      {modalIsOpen && (
        <Modal
        header={activeModal.header}
        closeButton={activeModal.closeButton}
        onClick={closeModal}
        text={activeModal.text}
        actions={
          <>
            <Button
              backgroundColor="#b3382c"
              text="Ok"
              onClick={() => {
                if (activeModal.id === 'basketModal') {
                  addToBasket();
                } else if (activeModal.id === 'deleteModal') {
                  deleteFromBasket();
                }
              }}
            />
            <Button
              backgroundColor="#b3382c"
              text="Cancel"
              onClick={closeModal}
            />
          </>
        }
      />
      )}
    </div>
  )

}

export default App;


// {/* <Routes>
//         <Route path="/" element={<ProductList arrProducts={arrProducts} openModal={openModal} modalId='basketModal' addToFavourites={addToFavourites} btnCardText='Add to cart'/>}/>
//         <Route path="/favourites" element={<FavouritesPage arrFavourites={arrFavourites} openModal={openModal} modalId='basketModal' addToFavourites={addToFavourites} btnCardText='Add to cart'/>}/>
//         <Route path="/basket" element={<BasketPage arrBasket={arrBasket} openModal={openModal} modalId='deleteModal' addToFavourites={addToFavourites} btnCardText='Delete' deleteButton={true} />}/>
//       </Routes> */}

// метод який додає товар якщо його немає в кошику, і видаляє якщо він є в кошику.
// const addToBasket = () => {
//   if (selectedProduct) {
//     if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
//       const newBasket = arrBasket.filter((product) => JSON.stringify(product) !== JSON.stringify(selectedProduct));
//       setArrBasket(newBasket);
//       localStorage.setItem("arrBasket", JSON.stringify(newBasket));
//     } else {
//       setArrBasket([...arrBasket, selectedProduct]);
//     }
//     setSelectedProduct(null);
//   }
//   closeModal();
//   return arrBasket;
// };


// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       modalIsOpen: false,
//       activeModal: null,
//       arrProducts: [],
//       arrFavourites: [],
//       arrBasket: [],
//       selectedProduct: null
//     };
//   }

//   componentDidMount() {
//     fetch('/products.json')
//     .then((response) => response.json())
//     .then((data) => {
//       this.setState({arrProducts: data});
//       console.log(data)
//     });

//     // перевірка при рендері (обране)
//     if (localStorage.getItem("arrFavourites") == null) {
//       localStorage.setItem(
//         "arrFavourites",
//         JSON.stringify(this.state.arrFavourites)
//       );
//     } else {
//       this.setState({
//         arrFavourites: JSON.parse(
//           localStorage.getItem("arrFavourites")
//         ),
//       });
//     }

//     // перевірка при рендері (кошик)
//     if (localStorage.getItem("arrBasket") == null) {
//       localStorage.setItem(
//         "arrBasket",
//         JSON.stringify(this.state.arrBasket)
//       );
//     } else {
//       this.setState({
//         arrBasket: JSON.parse(localStorage.getItem("arrBasket")),
//       });
//     }
//     console.log(this.state.arrFavourites)
//   }

//   openModal = (modalId, product) => {
//     const modal = modalConfigs.find(modal => modal.id === modalId);
//     this.setState({ 
//       modalIsOpen: true, 
//       activeModal: modal, 
//       selectedProduct: product
//     });
//   };

//   closeModal = () => {
//     this.setState({ modalIsOpen: false, activeModal: null });
//   };

//   addToBasket = () => {
//     const selectedProduct = this.state.selectedProduct;
//     if (selectedProduct) {
//       this.setState(prevState => ({
//         arrBasket: [...prevState.arrBasket, selectedProduct],
//         selectedProduct: null
//       }));
//     }
//     this.closeModal();
//   };

//   addToFavourites = (favouriteProduct) => {
//     const index = this.state.arrFavourites.findIndex((product) => {
//       return JSON.stringify(product) === JSON.stringify(favouriteProduct);
//     });
  
//     if (index === -1) {
//       this.setState({
//         arrFavourites: [
//           ...this.state.arrFavourites,
//           favouriteProduct,
//         ],
//       });
//       console.log('add');
//     } else {
//       const newFavourites = [
//         ...this.state.arrFavourites.slice(0, index),
//         ...this.state.arrFavourites.slice(index + 1),
//       ];
  
//       this.setState({
//         arrFavourites: newFavourites,
//       });
//     }
//   };

//   // оновлення local storage
//   componentDidUpdate() {
//     console.log(this.state.arrFavourites)
//     localStorage.setItem('arrFavourites', JSON.stringify(this.state.arrFavourites));
//     localStorage.setItem('arrBasket', JSON.stringify(this.state.arrBasket));
//   };

//   render() {
//     const {modalIsOpen, activeModal, arrProducts} = this.state;

//     return (
//       <div className="App">
//         <Header arrBasket={this.state.arrBasket} arrFavourites={this.state.arrFavourites}/>
//         <ProductList arrProducts={arrProducts} openModal={this.openModal} addToFavourites={this.addToFavourites}/>
//         {modalIsOpen && (
//           <Modal
//           header={activeModal.header}
//           closeButton={activeModal.closeButton}
//           onClick={this.closeModal}
//           text={activeModal.text}
//           actions={
//             <>
//               <Button
//                 backgroundColor="#b3382c"
//                 text="Ok"
//                 onClick={() => {
//                   this.addToBasket();
//                   console.log(this.state.arrBasket);
//                 }}
//               />
//               <Button
//                 backgroundColor="#b3382c"
//                 text="Cancel"
//                 onClick={this.closeModal}
//               />
//             </>
//           }
//         />
//         )}
//       </div>
//     )
//   }
// }

// export default App;
















// const App = ()=> {

//   const [modalIsOpen, setModalIsOpen] = useState(false);
//   const [activeModal, setActiveModal] = useState(null);
//   const [selectedProduct, setSelectedProduct] = useState(null);
//   const [arrProducts, setArrProducts] = useState([]);
//   const [arrFavourites, setArrFavourites] = useState([]);
//   const [arrBasket, setArrBasket] = useState([]);

//   useEffect(()=> {

//     fetch('/products.json')
//     .then((response) => response.json())
//     .then((data) => {
//       setArrProducts(data);
//     });

//     const localFavourites = JSON.parse(localStorage.getItem('arrFavourites'));
//     if (!localFavourites) {
//       console.log(localFavourites)
//       localStorage.setItem('arrFavourites', JSON.stringify([]));
//     } else {
//       setArrFavourites(localFavourites);
//     }

//     const localBasket = JSON.parse(localStorage.getItem('arrBasket'));
//     if (!localBasket) {
//       localStorage.setItem('arrBasket', JSON.stringify([]));
//     } else {
//       setArrBasket(localBasket);
//     }

//     // const arrFavouritesStorage = localStorage.getItem('arrFavourites');
//     // if (arrFavouritesStorage === null) {
//     //   localStorage.setItem('arrFavourites', JSON.stringify(arrFavourites));
//     // } else {
//     //   setArrFavourites(JSON.parse(arrFavouritesStorage));
//     // }

//     // const arrBasketStorage = localStorage.getItem('arrBasket');
//     // if (arrBasketStorage === null) {
//     //   localStorage.setItem('arrBasket', JSON.stringify(arrBasket));
//     // } else {
//     //   setArrBasket(JSON.parse(arrBasketStorage));
//     // }

//   }, []);


// return - до моменту додавання рутів
// return (
//   <div className="App">
//     <Header arrBasket={arrBasket} arrFavourites={arrFavourites}/>
//     <ProductList arrProducts={arrProducts} openModal={openModal} addToFavourites={addToFavourites}/>
//     {modalIsOpen && (
//       <Modal
//       header={activeModal.header}
//       closeButton={activeModal.closeButton}
//       onClick={closeModal}
//       text={activeModal.text}
//       actions={
//         <>
//           <Button
//             backgroundColor="#b3382c"
//             text="Ok"
//             onClick={() => {
//               addToBasket();
//               console.log(arrBasket);
//             }}
//           />
//           <Button
//             backgroundColor="#b3382c"
//             text="Cancel"
//             onClick={closeModal}
//           />
//         </>
//       }
//     />
//     )}
//   </div>
// )
