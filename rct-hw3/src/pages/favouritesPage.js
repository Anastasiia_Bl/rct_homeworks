import ProductCard from "../components/productCard/productCard";


export const FavouritesPage = (props) => {

    const {arrFavourites, openModal, addToFavourites, modalId, btnCardText} = props;
    const productCards = arrFavourites.map(item => <ProductCard key={item.id} product={item} openModal={openModal} modalId={modalId} addToFavourites={addToFavourites} btnCardText={btnCardText}/>);

    return (
        <ul className="products__list">
            {productCards}
        </ul>
    );
}