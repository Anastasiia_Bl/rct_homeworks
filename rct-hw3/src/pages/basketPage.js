import ProductCard from "../components/productCard/productCard";

export const BasketPage = (props) => {
  const {arrBasket, openModal, addToFavourites, modalId, btnCardText, deleteButton} = props;

  const productCards = arrBasket.map((item) => (
    <ProductCard
      key={item.id}
      product={item}
      openModal={openModal}
      addToFavourites={addToFavourites}
      modalId={modalId}
      btnCardText={btnCardText}
      deleteButton={deleteButton}
    />
  ));

  return <ul className="products__list">{productCards}</ul>;
};
