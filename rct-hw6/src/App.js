

import React, { useEffect, useState } from 'react';
import './App.css';
import Button from './components/button/button.js';
import Modal from './components/modal/modal.js';
import Header from "./components/header/header";
import { modalConfigs } from "./arrModalConfigues";
import { Router } from './components/router/router';
import { useDispatch, useSelector } from "react-redux";
import { openModalAction, closeModalAction } from './redux/modal/action';
import { getProducts } from './redux/products/action';
import { addToBasketAction, deleteFromBasketAction } from './redux/basket/action';

import { DisplayContext } from './context/displayContext';



const App = () => {
  const dispatch = useDispatch();
  const {activeModal, modalIsOpen, selectedProduct} = useSelector((state) => state.modal);
  const arrFavourites = useSelector((state) => state.favourites.arrFavourites);
  const arrBasket = useSelector((state) => state.basket.arrBasket);
  const [display, setDisplay] = useState('cards');


  useEffect(() => {
    dispatch(getProducts());
  
  }, []);
  
  const openModal = (modalId, product) => {
    const modal = modalConfigs.find(modal => modal.id === modalId);
    console.log(modal)
    console.log(modalIsOpen)
    dispatch(
      openModalAction({ product: product, modal: modal })
    );
  };

  const closeModal = () => {
    dispatch(closeModalAction());
  };

  const addToBasket = () => {
    if (selectedProduct) {
      if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
        closeModal();
        return arrBasket; // повертаю масив продуктів без змін
      } else {
        dispatch(addToBasketAction([...arrBasket, selectedProduct]));
        localStorage.setItem("arrBasket", JSON.stringify([...arrBasket, selectedProduct]));
      }
    }
    closeModal();
    return arrBasket;
  };


  const deleteFromBasket = () => {
  if (selectedProduct) {
    if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
      const newBasket = arrBasket.filter((product) => JSON.stringify(product) !== JSON.stringify(selectedProduct));
      dispatch(deleteFromBasketAction(newBasket));
      localStorage.setItem("arrBasket", JSON.stringify(newBasket));
    } 
  }
  closeModal();
  return arrBasket;
};


   // оновлення local storage
   useEffect(() => {
    console.log(arrFavourites);
    localStorage.setItem("arrFavourites", JSON.stringify(arrFavourites));
    localStorage.setItem("arrBasket", JSON.stringify(arrBasket));
  }, [arrFavourites, arrBasket]);

  const switchDisplay = (displayType) => {
    setDisplay(displayType);
  }

  return (
    <DisplayContext.Provider value={{display, switchDisplay}}>
    <div className="App">
      <Header/>
      <Router openModal={openModal} />
      
      {modalIsOpen && (
        <Modal
        header={activeModal.header}
        closeButton={activeModal.closeButton}
        onClick={closeModal}
        text={activeModal.text}
        actions={
          <>
            <Button
              backgroundColor="#b3382c"
              text="Ok"
              onClick={() => {
                if (activeModal.id === 'basketModal') {
                  addToBasket();
                } else if (activeModal.id === 'deleteModal') {
                  deleteFromBasket();
                }
              }}
            />
            <Button
              backgroundColor="#b3382c"
              text="Cancel"
              onClick={closeModal}
            />
          </>
        }
      />
      )}
    </div>
    </DisplayContext.Provider>
  )

}

export default App;




// метод який додає товар якщо його немає в кошику, і видаляє якщо він є в кошику.
// const addToBasket = () => {
//   if (selectedProduct) {
//     if (arrBasket.some((product) => JSON.stringify(product) === JSON.stringify(selectedProduct))) {
//       const newBasket = arrBasket.filter((product) => JSON.stringify(product) !== JSON.stringify(selectedProduct));
//       setArrBasket(newBasket);
//       localStorage.setItem("arrBasket", JSON.stringify(newBasket));
//     } else {
//       setArrBasket([...arrBasket, selectedProduct]);
//     }
//     setSelectedProduct(null);
//   }
//   closeModal();
//   return arrBasket;
// };


