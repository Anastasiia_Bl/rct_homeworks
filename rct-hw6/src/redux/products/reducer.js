import {productsTypes} from './types';


// const initialState = [];

// export function productsReducer(state = initialState, action) {
//   switch (action.type) {
//     case productsTypes.GET_PRODUCTS:
//       return action.payload;
//     default:
//       return state;
//   }
// }


const productsInitialState = {
  isLoading: false,
  products: [],
  error: null,
};

export const productsReducer = (state = productsInitialState, action) => {
  switch (action.type) {
    case productsTypes.GET_PRODUCTS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };
    case productsTypes.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        products: action.payload,
      };
    case productsTypes.GET_PRODUCTS_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};