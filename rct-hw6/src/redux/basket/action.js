import { basketTypes } from "./types";

export const addToBasketAction = (payload) => ({
    type: basketTypes.ADD_TO_BASKET,
    payload,
  });

  export const deleteFromBasketAction = (payload) => ({
    type: basketTypes.DELETE_FROM_BASKET,
    payload,
  });

  export const cleanBasketAction = () => ({
    type: basketTypes.CLEAN_BASKET,
  });