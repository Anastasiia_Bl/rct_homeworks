import { createStore, applyMiddleware } from 'redux';
import {rootReducer} from "./rootReduser";
import thunk from 'redux-thunk';



export const store = createStore(
    rootReducer, applyMiddleware(thunk)
  );
  