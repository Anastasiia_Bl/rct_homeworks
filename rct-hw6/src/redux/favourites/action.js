import { favouritesTypes } from "./types";


export const addFavouritesAction = (payload) => ({
    type: favouritesTypes.ADD_FAVOURITES,
    payload,
  });

  export const deleteFavouritesAction = (payload) => ({
    type: favouritesTypes.DELETE_FAVOURITES,
    payload,
  });