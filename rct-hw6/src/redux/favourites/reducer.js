import { favouritesTypes } from "./types";
import { getProductsFromLS } from "../../utils";

const lsFavorites = getProductsFromLS("arrFavourites");

const initialState = {
    arrFavourites: lsFavorites,
  };

export const toggleFavouritesReducer = (state = initialState, action) => {
    switch (action.type) {
      case favouritesTypes.ADD_FAVOURITES:
        return {
            ...state,
            arrFavourites: action.payload,
        }

      case favouritesTypes.DELETE_FAVOURITES:
        return {
            ...state,
            arrFavourites: action.payload,
        }

      default:
        return state;
    }
  };
  