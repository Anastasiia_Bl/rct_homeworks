import { combineReducers } from "redux";
import { modalReducer as modal } from "./modal/reducer";
import { productsReducer as products } from "./products/reducer";
import { toggleFavouritesReducer as favourites } from "./favourites/reducer";
import { basketReducer as basket } from "./basket/reducer";

export const rootReducer = combineReducers({
    
    modal, products, favourites, basket,
  });