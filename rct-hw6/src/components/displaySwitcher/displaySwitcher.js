import { useContext } from "react";
import { DisplayContext } from "../../context/displayContext";
import './displaySwitcher.scss';

export default function DisplaySwitcher() {

    const { switchDisplay } = useContext(DisplayContext);

    return (
        <div className="displaySwitcher__container">
            <button onClick={()=> switchDisplay('cards')} >
                <img src="./img/cards-bold-svgrepo-com.svg" alt="img-card"/>
            </button>
            <button onClick={()=> switchDisplay('table')}>
                <img src="./img/list-svgrepo-com.svg" alt="img-table"/>
            </button>
        </div>
    )

}