import { Route, Routes } from "react-router-dom";
import { FavouritesPage } from '../../pages/favouritesPage';
import { BasketPage } from '../../pages/basketPage';
import { Home } from '../../pages/homePage';
// import ProductList from "../productList/productList";
// import { useSelector } from "react-redux";



export function Router(props) {

    const { openModal } = props;
    // const arrProducts = useSelector((state) => state.products.products);

    return (
        <Routes>
        <Route path="/" element={<Home openModal={openModal} modalId='basketModal' btnCardText='Add to cart'/>}/>
        <Route path="/favourites" element={<FavouritesPage openModal={openModal} modalId='basketModal' btnCardText='Add to cart'/>}/>
        <Route path="/basket" element={<BasketPage openModal={openModal} modalId='deleteModal' btnCardText='Delete' deleteButton={true} />}/>
      </Routes>
    )
}