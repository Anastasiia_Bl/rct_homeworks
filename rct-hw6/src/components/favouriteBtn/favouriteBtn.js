import { useEffect, useState } from "react";
import "./favouriteBtn.scss";
import star from "./star.svg";
import starFav from "./starFavourite.svg";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from 'react-redux';
import { addFavouritesAction, deleteFavouritesAction } from '../../redux/favourites/action';

// кнопка (зірочка) додати в обране
 const FavouriteBtn = (props) => {

  const [onClick, setOnClick] = useState(false);
  const dispatch = useDispatch();
  const arrFavourites = useSelector((state) => state.favourites.arrFavourites);
  const { favouriteProduct } = props; 
  

  const addToFavourites = (favouriteProduct) => {
    console.log(arrFavourites)
    const index = arrFavourites.findIndex((product) => {
      return JSON.stringify(product) === JSON.stringify(favouriteProduct);
    });

    if (index === -1) {
      dispatch(addFavouritesAction([...arrFavourites, favouriteProduct]));
      console.log("add");
    } else {
      const newFavourites = [
        ...arrFavourites.slice(0, index),
        ...arrFavourites.slice(index + 1),
      ];
      dispatch(deleteFavouritesAction(newFavourites));
    }
  };

  useEffect(() => {
    const localFavourites = JSON.parse(localStorage.getItem("arrFavourites"));

    localFavourites.forEach((element) => {
      if (JSON.stringify(element) === JSON.stringify(favouriteProduct)) {
        setOnClick(true);
      }
    });
  }, [favouriteProduct]);

  return (
    <button
      className="product__favourite-btn"
      href="/#"
      onClick={() => {
        addToFavourites(favouriteProduct);
        setOnClick(!onClick);
      }}
    >
      {onClick ? (
        <img src={starFav} alt="img" />
      ) : (
        <img src={star} alt="img" />
      )}
    </button>
  );

}

FavouriteBtn.propTypes = {
  addToFavourites: PropTypes.func,
  favouriteProduct: PropTypes.object,
};

export default FavouriteBtn;

// кнопка (зірочка) додати в обране
// export class FavouriteBtn extends Component {
//   constructor() {
//     super();
//     this.state = {
//       onClick: false,
//     };
//   }
//   componentDidMount = () => {
//     JSON.parse(localStorage.getItem("arrFavourites")).forEach((element) => {
//       if (
//         JSON.stringify(element) === JSON.stringify(this.props.favouriteProduct)
//       ) {
//         this.setState({
//           onClick: !this.state.onClick,
//         });
//       }
//     });
//   };
//   render() {
//     const { addToFavourites, favouriteProduct } = this.props;
//     return (
//       <a
//         className="product__favourite-btn"
//         href="/#"
//         onClick={() => {
//           addToFavourites(favouriteProduct);
//           this.setState({
//             onClick: !this.state.onClick,
//           });
//         }}
//       >
//         {this.state.onClick ? (
//           <img src={starFav} alt="img" />
//         ) : (
//           <img src={star} alt="img" />
//         )}
//       </a>
//     );
//   }
// }



