
import "../productList/productList.scss";
import ProductCard from "../productCard/productCard";
import PropTypes from "prop-types";
// import { useSelector } from "react-redux";
import { useContext } from "react";
import { DisplayContext } from "../../context/displayContext";


const ProductList = (props) => {
    // const arrProducts = useSelector((state) => state.products.products);
    const { display } = useContext(DisplayContext);

    const {arrProducts, openModal, modalId, btnCardText, deleteButton} = props;
    const productCards = arrProducts.map(item => <ProductCard key={item.id} product={item} openModal={openModal} modalId={modalId} btnCardText={btnCardText} deleteButton={deleteButton}/>);

    return (
        <>
        {display === 'cards' ? (
            <ul className="products__list">
                {productCards}
            </ul>
        ) : (
            <ul className="products__table">
                {productCards}
            </ul>
        )}
        </>
    );
}

ProductList.propTypes = {
    arrProducts: PropTypes.array,
    openModal: PropTypes.func,
    addToFavourites: PropTypes.func,
}

export default ProductList;

