import Modal from "../modal/modal";
import { modalConfigs } from "../../arrModalConfigues";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";
import { modalReducer } from "../../redux/modal/reducer";
import { openModalAction, closeModalAction } from "../../redux/modal/action";
// import configureStore from 'redux-mock-store';
// import { Provider } from "react-redux";
// import { DisplayContext } from "../../context/displayContext";
// import Button from "../button/button";
// import ProductCard from "../productCard/productCard";


afterEach(cleanup);

test('should component exist', ()=> {
    render(<Modal/>);
    expect(screen.getByTestId('modal')).toBeInTheDocument()
});

test('testing modal basket props', ()=> {
    const modalProps = {
        id: 'basketModal',
        header: modalConfigs.header,
        closeButton: modalConfigs.closeButton,
        text: modalConfigs.text,
    }
    render(<Modal {...modalProps}/>);
    expect(screen.getByTestId('modal')).toHaveTextContent("Do you want to add to basket this item?")
    expect(screen.getByTestId('modal')).toHaveTextContent("This item will be in your basket and you can continue your shopping.")
})

test('testing modal delete props', ()=> {
    const modalProps = {
        id: 'deleteModal',
        header: "Do you want to delete this item from Basket?",
        closeButton: true,
        text: "Once you delete this file, it won`t be in your Basket. Are you sure you want to delete it?",
    }
    render(<Modal {...modalProps}/>);
    expect(screen.getByTestId('modal-main')).toHaveTextContent("Do you want to delete this item from Basket?")
    expect(screen.getByTestId('modal-main')).toHaveTextContent("Once you delete this file, it won`t be in your Basket. Are you sure you want to delete it?")
})


test('snapshot modal', ()=> {
    const tree = renderer.create(<Modal />).toJSON;
    expect(tree).toMatchSnapshot();
})

// тест ред'юсера Модального вікна
test("should open with click, set active modal and selected product", () => {
    const initialState = {
      modalIsOpen: false,
      activeModal: null,
      selectedProduct: null,
    };
    const modal = {
      id: "modalId",
      header: "Modal Header",
      closeButton: true,
      text: "Modal Text",
    };
    const product = { id: 1, title: "Illy" };
    const action = openModalAction({ modal, product });

    const newState = modalReducer(initialState, action);

    expect(newState.modalIsOpen).toBe(true);
    expect(newState.activeModal).toEqual(modal);
    expect(newState.selectedProduct).toEqual(product);
  });

  test("should close with click, reset active modal", () => {
    const initialState = {
      modalIsOpen: true,
      activeModal: {
        id: "modalId",
        header: "Modal Header",
        closeButton: true,
        text: "Modal Text",
      },
      selectedProduct: { id: 1, name: "Illy" },
    };
    const action = closeModalAction();

    const newState = modalReducer(initialState, action);

    expect(newState.modalIsOpen).toBe(false);
    expect(newState.activeModal).toBeNull();
  });


// тест ред'юсера
// jest.mock('../favouriteBtn/favouriteBtn', () => () => <div data-testid="favourite-btn-mock" />);
// let mockStore = configureStore();
// let store;

// beforeEach( ()=> {
//     store = mockStore({modal: {modalIsOpen: false}})
// })

// test('modal should open with click', ()=> {
//     const product = {
//         title: "Barista",
//         price: "500",
//         image: "./img/barista.jpg",
//         id: "1",
//         color: "black"
//       };
//       const openModalMock = jest.fn(() => {
//         store.dispatch({ type: 'OPEN_MODAL' });
//       });

//     render(
//         <Provider store={store}>
//         <DisplayContext.Provider value={{ display: 'cards' }}>
//             <Modal/>
//             <ProductCard btnCardText='Add to cart' product={product} openModal={openModalMock}/>
//         </DisplayContext.Provider>
//         </Provider>
//     );
//     const button = screen.getByText('Add to cart');
//     expect(button).toBeInTheDocument();
//     fireEvent.click(button);
//     const actions = store.getActions();
//     expect(actions).toEqual([{type: 'OPEN_MODAL'}])
// })


// test('modal should close with click', ()=> {
    
//       const closeModalMock = jest.fn(() => {
//         store.dispatch({ type: 'CLOSE_MODAL' });
//       });

//     render(
//         <Provider store={store}>
//         <DisplayContext.Provider value={{ display: 'cards' }}>
//             <Modal onClick={closeModalMock}/>
//             <Button text="Cancel" onClick={closeModalMock}/>
//         </DisplayContext.Provider>
//         </Provider>
//     );
//     const btnCancel = screen.getByText('Cancel');
//     fireEvent.click(btnCancel);
//     const actions = store.getActions();
//     expect(actions).toEqual([{type: 'CLOSE_MODAL'}])
// })

// інші тести
// test('displays correct header based on basket activeModal', () => {
//     const activeModal = 'basketModal';
//     const { getByText } = render(<Modal activeModal={activeModal} />);
//     const headerElement = getByText('Do you want to add to basket this item?');
//     expect(headerElement).toBeInTheDocument();
//   });

//   test('displays correct header based on delete activeModal', () => {
//     const activeModal = 'deleteModal';
//     const { getByText } = render(<Modal activeModal={activeModal} />);
//     const headerElement = getByText('Do you want to delete this item from Basket?');
//     expect(headerElement).toBeInTheDocument();
//   });