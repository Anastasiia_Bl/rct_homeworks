import { getProductsRequested, getProductsSuccess, getProductsError } from "../../redux/products/action";
import { productsReducer } from "../../redux/products/reducer";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test("should requested products", () => {
    const initialState = {
      isLoading: false,
      products: [],
      error: null,
    };
    const action = getProductsRequested();

    const newState = productsReducer(initialState, action);

    expect(newState.isLoading).toBe(true);
  });

  test("should get products", () => {
    const initialState = {
      isLoading: true,
      products: [],
      error: null,
    };
    const products = [{ id: 1, name: "Illy" }, { id: 2, name: "Barista" }];
    const action = getProductsSuccess(products);

    const newState = productsReducer(initialState, action);

    expect(newState.isLoading).toBe(false);
    expect(newState.products).toEqual(products);
  });

  test("should show error trying get products", () => {
    const initialState = {
      isLoading: true,
      products: [],
      error: null,
    };
    const error = "Error";
    const action = getProductsError(error);

    const newState = productsReducer(initialState, action);

    expect(newState.isLoading).toBe(false);
    expect(newState.error).toBe(error);
  });