import { render, screen, cleanup, fireEvent } from "@testing-library/react";
// import renderer from "react-test-renderer";
// import configureStore from 'redux-mock-store';
// import { Provider } from "react-redux";
// import { DisplayContext } from "../../context/displayContext";
import { addToBasketAction, deleteFromBasketAction, cleanBasketAction } from "../../redux/basket/action";
import { basketReducer } from "../../redux/basket/reducer";

afterEach(cleanup);

test("should add product to basket", () => {
    const initialState = { arrBasket: [] };
    const product = { id: 1, name: "Illy" };
    const action = addToBasketAction([product]);

    const newState = basketReducer(initialState, action);

    expect(newState.arrBasket).toEqual([product]);
  });

  test("should delete product from basket", () => {
    const initialState = { arrBasket: [{ id: 1, name: "Illy" }] };
    const action = deleteFromBasketAction([]);

    const newState = basketReducer(initialState, action);

    expect(newState.arrBasket).toEqual([]);
  });

  test("should clean basket", () => {
    const initialState = { arrBasket: [{ id: 1, name: "Illy" }] };
    const action = cleanBasketAction();

    const newState = basketReducer(initialState, action);

    expect(newState.arrBasket).toEqual([]);
  });