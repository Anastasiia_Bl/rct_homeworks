import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import Button from "../button/button";
import renderer from "react-test-renderer";

afterEach(cleanup);

// snapshot test
test('snapshot component Button exist', ()=> {
    const button = renderer.create(<Button />).toJSON;
    expect(button).toMatchSnapshot();
});

// unit test
test('button render with text to add to Basket', ()=> {
    render(<Button text='Add to cart'/>)
    expect(screen.getByText('Add to cart')).toBeInTheDocument();
})

test('button render with text to delete from Basket', ()=> {
    render(<Button text='Delete'/>)
    expect(screen.getByText('Delete')).toBeInTheDocument();
})

// unit test, testing onClick
test('calls onClick when button is clicked', () => {
    const onClickMock = jest.fn(); // Создаем мок-функцию для onClick

    render(
      <Button
        backgroundColor="black"
        text="Add to cart"
        onClick={onClickMock}
      />
    );

    const button = screen.getByText('Add to cart');
    fireEvent.click(button); // Симулируем клик на кнопку

    expect(onClickMock).toHaveBeenCalledTimes(1); // Проверяем, что onClick был вызван один раз
  });
