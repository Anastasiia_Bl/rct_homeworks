import { cleanup } from "@testing-library/react";
import { addFavouritesAction, deleteFavouritesAction } from "../../redux/favourites/action";
import { toggleFavouritesReducer } from "../../redux/favourites/reducer";

afterEach(cleanup);

test("should add product to favourites", () => {
    const initialState = { arrFavourites: [] };
    const product = { id: 1, name: "Illy" };
    const action = addFavouritesAction([product]);

    const newState = toggleFavouritesReducer(initialState, action);

    expect(newState.arrFavourites).toEqual([product]);
  });

  test("should delete product from favourites", () => {
    const initialState = { arrFavourites: [{ id: 1, name: "Illy" }] };
    const action = deleteFavouritesAction([]);

    const newState = toggleFavouritesReducer(initialState, action);

    expect(newState.arrFavourites).toEqual([]);
  });
