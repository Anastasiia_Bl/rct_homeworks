
import "./favourites.scss";
import PropTypes from "prop-types";
import star from './img/star.svg'
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";


const Favourites = (props) => {

  const arrFavourites = useSelector((state) => state.favourites.arrFavourites);

  return (
    <NavLink to="/favourites" className="header__favourites">
    <img src={star} alt="img" />
    {arrFavourites.length}
  </NavLink>
  );

}

Favourites.propTypes = {
  arrFavourites: PropTypes.array,
};


export default Favourites;

// class Favourites extends Component {

//   render() {
//     return (
//       <a className="header__favourites" href="/#">
//         <img src={star} alt="img" />
//         {this.props.arrFavourites.length}
//       </a>
//     );
//   }
// }




