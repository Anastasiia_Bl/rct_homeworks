// import ProductCard from "../components/productCard/productCard";
import { useSelector } from "react-redux";
import ProductList from "../components/productList/productList";

export const FavouritesPage = (props) => {
    const arrFavourites = useSelector((state) => state.favourites.arrFavourites);
  
    return <ProductList arrProducts={arrFavourites} {...props} />;
  };

// export const FavouritesPage = (props) => {

//     const { openModal, modalId, btnCardText} = props;
//     const arrFavourites = useSelector((state) => state.favourites.arrFavourites);

//     const productCards = arrFavourites.map(item => <ProductCard key={item.id} product={item} openModal={openModal} modalId={modalId} btnCardText={btnCardText}/>);

//     return (
//         <ul className="products__list">
//             {productCards}
//         </ul>
//     );
// }

