import { useSelector } from "react-redux";
import ProductList from "../components/productList/productList";
import DisplaySwitcher from "../components/displaySwitcher/displaySwitcher";

export const Home = (props) => {
    const arrProducts = useSelector((state) => state.products.products);

    return (
    <>
    <DisplaySwitcher />
    <ProductList arrProducts={arrProducts} {...props} />
    </>
    );
}