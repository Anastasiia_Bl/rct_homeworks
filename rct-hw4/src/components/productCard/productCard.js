
import '../productCard/productCard.scss'
import Button from "../button/button";
import { FavouriteBtn } from "../favouriteBtn/favouriteBtn";
import PropTypes from "prop-types";



const ProductCard = (props) => {

  const { product: { id, title, price, image, color }, deleteButton} = props;

  return (
    <>
  <li className="product__item">
          {deleteButton && (
            <button className="product__delete-button" onClick={() => {props.openModal(props.modalId, props.product);
              console.log(props.product)}}></button>
          )}
    <a className="product__link" href="/#">
      <div className="product__img-wrapper">
        <img className="product__img" src={image} alt={title} />
      </div>
      <h3 className="product__title">{title}</h3>
    </a>
    <p className="product__info">
      <span>Item number: {id} </span>
      <span>Color: {color}</span>
    </p>
    <div className="product__purchase">
      <p className="product__item-price">Price: {price}₴</p>
      <FavouriteBtn favouriteProduct={props.product}/>
    </div>
      <Button
      backgroundColor="black"
      text={props.btnCardText}
      onClick={() => {props.openModal(props.modalId, props.product);
    console.log(props.product)}}
    />
  </li>
    </>
);
}

ProductCard.propTypes = {
  product: PropTypes.object,
  favouriteProduct: PropTypes.object,
  openModal: PropTypes.func,
}

export default ProductCard;


// class ProductCard extends Component {

//   render() {
//     const { product: { id, title, price, image, color }} = this.props;

//     return (
//         <>
//       <li className="product__item">
//         <a className="product__link" href="/#">
//           <div className="product__img-wrapper">
//             <img className="product__img" src={image} alt={title} />
//           </div>
//           <h3 className="product__title">{title}</h3>
//         </a>
//         <p className="product__info">
//           <span>Item number: {id} </span>
//           <span>Color: {color}</span>
//         </p>
//         <div className="product__purchase">
//           <p className="product__item-price">Price: {price}₴</p>
//           <FavouriteBtn addToFavourites={this.props.addToFavourites} favouriteProduct={this.props.product}/>
//         </div>
//         <Button
//           backgroundColor="black"
//           text="Add to cart"
//           onClick={() => {this.props.openModal('basketModal', this.props.product);
//         console.log(this.props.product)}}
//         />
//       </li>
//         </>
//     );
//   }
// }

