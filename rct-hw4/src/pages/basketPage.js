// import ProductCard from "../components/productCard/productCard";
import { useSelector } from "react-redux";
import ProductList from "../components/productList/productList";

export const BasketPage = (props) => {
  const arrBasket = useSelector((state) => state.basket.arrBasket);

  return <ProductList arrProducts={arrBasket} {...props} />;
};

// export const BasketPage = (props) => {
//   const { openModal, modalId, btnCardText, deleteButton} = props;
//   const arrBasket = useSelector((state) => state.basket.arrBasket);

//   const productCards = arrBasket.map((item) => (
//     <ProductCard
//       key={item.id}
//       product={item}
//       openModal={openModal}
//       modalId={modalId}
//       btnCardText={btnCardText}
//       deleteButton={deleteButton}
//     />
//   ));

//   return <ul className="products__list">{productCards}</ul>;
// };

