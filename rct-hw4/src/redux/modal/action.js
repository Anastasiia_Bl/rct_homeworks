import { modalTypes } from './type';

export function openModalAction(payload) {
    return {
        type: modalTypes.OPEN_MODAL,
        payload,
    }
}

export function closeModalAction() {
    return {
        type: modalTypes.CLOSE_MODAL,
    }
}


