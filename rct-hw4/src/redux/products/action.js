import {productsTypes} from './types';


// export function getProducts(data) {
//     return {
//       type: productsTypes.GET_PRODUCTS,
//       payload: data,
//     };
//   }
  

//   export function getProductsAsync() {
//     return async function (dispatch) {
//       const { results } = await fetch("/products.json").then((res) =>
//         res.json()
//       );
//       dispatch(getProducts(results));
//     };
//   }
  


  export const getProductsRequested = () => ({
    type: productsTypes.GET_PRODUCTS_REQUESTED,
  });
  
  export const getProductsSuccess = (products) => ({
    type: productsTypes.GET_PRODUCTS_SUCCESS,
    payload: products,
  });
  
  export const getProductsError = (error) => ({
    type: productsTypes.GET_PRODUCTS_ERROR,
    payload: error,
  });
  
  export const getProducts = () => {
    return async (dispatch) => {
      dispatch(getProductsRequested());
      try {
        const response = await fetch("/products.json");
        const data = await response.json();
        dispatch(getProductsSuccess(data));
      } catch (error) {
        dispatch(getProductsError(error));
      }
    };
  };

