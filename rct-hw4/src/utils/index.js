

export const getProductsFromLS = (key) => {
    const lsProducts = localStorage.getItem(key);
    if (!lsProducts) return [];
    try {
      const value = JSON.parse(lsProducts);
      return value;
    } catch (e) {
      return [];
    }
  };