
import { Component } from "react";
import './App.css';
import Button from './components/button/button.js';
import Modal from './components/modal/modal.js';
import Header from "./components/header/header";
import ProductList from "./components/productList/productList";
import { modalConfigs } from "./arrModalConfigues";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      activeModal: null,
      arrProducts: [],
      arrFavourites: [],
      arrBasket: [],
      selectedProduct: null
    };
  }

  static getProductsFromLS(key) {
    const lsProducts = localStorage.getItem(key);
    if (!lsProducts) return [];
    try {
      const value = JSON.parse(lsProducts);
      return value;
    } catch (e) {
      return [];
    }
  }

  componentDidMount() {
    fetch('/products.json')
      .then((response) => response.json())
      .then((data) => {
        this.setState({ arrProducts: data });
        console.log(data);
      });

    const lsFavorites = App.getProductsFromLS("arrFavourites");
    const lsBasket = App.getProductsFromLS("arrBasket");
    this.setState({
      arrFavourites: lsFavorites,
      arrBasket: lsBasket,
    });
    console.log(lsFavorites);
  }
  // componentDidMount() {
  //   fetch('/products.json')
  //   .then((response) => response.json())
  //   .then((data) => {
  //     this.setState({arrProducts: data});
  //     console.log(data)
  //   });

  //   // перевірка при рендері (обране)
  //   if (localStorage.getItem("arrFavourites") == null) {
  //     localStorage.setItem(
  //       "arrFavourites",
  //       JSON.stringify(this.state.arrFavourites)
  //     );
  //   } else {
  //     this.setState({
  //       arrFavourites: JSON.parse(
  //         localStorage.getItem("arrFavourites")
  //       ),
  //     });
  //   }

  //   // перевірка при рендері (кошик)
  //   if (localStorage.getItem("arrBasket") == null) {
  //     localStorage.setItem(
  //       "arrBasket",
  //       JSON.stringify(this.state.arrBasket)
  //     );
  //   } else {
  //     this.setState({
  //       arrBasket: JSON.parse(localStorage.getItem("arrBasket")),
  //     });
  //   }
  //   console.log(this.state.arrFavourites)
  // }

  openModal = (modalId, product) => {
    const modal = modalConfigs.find(modal => modal.id === modalId);
    this.setState({ 
      modalIsOpen: true, 
      activeModal: modal, 
      selectedProduct: product
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, activeModal: null });
  };

  addToBasket = () => {
    const selectedProduct = this.state.selectedProduct;
    if (selectedProduct) {
      this.setState(prevState => ({
        arrBasket: [...prevState.arrBasket, selectedProduct],
        selectedProduct: null
      }));
    }
    this.closeModal();
  };

  addToFavourites = (favouriteProduct) => {
    const index = this.state.arrFavourites.findIndex((product) => {
      return JSON.stringify(product) === JSON.stringify(favouriteProduct);
    });
  
    if (index === -1) {
      this.setState({
        arrFavourites: [
          ...this.state.arrFavourites,
          favouriteProduct,
        ],
      });
      console.log('add');
    } else {
      const newFavourites = [
        ...this.state.arrFavourites.slice(0, index),
        ...this.state.arrFavourites.slice(index + 1),
      ];
  
      this.setState({
        arrFavourites: newFavourites,
      });
    }
  };

  // оновлення local storage
  // componentDidUpdate() {
  //   console.log(this.state.arrFavourites)
  //   // localStorage.arrFavourites = JSON.stringify(this.state.arrFavourites);
  //   // localStorage.arrBasket = JSON.stringify(this.state.arrBasket);
  //   localStorage.setItem('arrFavourites', JSON.stringify(this.state.arrFavourites));
  //   localStorage.setItem('arrBasket', JSON.stringify(this.state.arrBasket));
  // };
  componentDidUpdate(prevState) {
    if (prevState.arrFavourites !== this.state.arrFavourites) {
      localStorage.setItem('arrFavourites', JSON.stringify(this.state.arrFavourites));
    }
    if (prevState.arrBasket !== this.state.arrBasket) {
      localStorage.setItem('arrBasket', JSON.stringify(this.state.arrBasket));
    }
  }

  render() {
    const {modalIsOpen, activeModal, arrProducts} = this.state;

    return (
      <div className="App">
        <Header arrBasket={this.state.arrBasket} arrFavourites={this.state.arrFavourites}/>
        <ProductList arrProducts={arrProducts} openModal={this.openModal} addToFavourites={this.addToFavourites}/>
        {modalIsOpen && (
          <Modal
          header={activeModal.header}
          closeButton={activeModal.closeButton}
          onClick={this.closeModal}
          text={activeModal.text}
          actions={
            <>
              <Button
                backgroundColor="#b3382c"
                text="Ok"
                onClick={() => {
                  this.addToBasket();
                  console.log(this.state.arrBasket);
                }}
              />
              <Button
                backgroundColor="#b3382c"
                text="Cancel"
                onClick={this.closeModal}
              />
            </>
          }
        />
        )}
      </div>
    )
  }
}

export default App;



 // addToFavourites = (favouriteProduct) => {
  //   if (!this.state.arrFavourites.includes(favouriteProduct)) {
  //     this.setState({
  //       arrFavourites: [
  //         ...this.state.arrFavourites,
  //         favouriteProduct,
  //       ],
  //     });
  //     console.log(1);
  //     console.log(this.state.arrFavourites)
  //   } else {
  //     this.setState({
  //       arrFavourites: this.state.arrFavourites.filter(
  //         (product) => {
  //           if (JSON.stringify(product) === JSON.stringify(favouriteProduct)) {
  //           return false;
  //           } else {
  //             return true;
  //           }
  //         }
  //       ),
  //     });
  //   }
  // };


    // addToBasket = () => {
  //   const { arrProducts, arrBasket } = this.state;
  //   const selectedProduct = arrProducts.find(
  //     (product) => product.id === this.state.selectedProductId
  //   );
  //   if (selectedProduct) {
  //     const updatedBasket = [...arrBasket, selectedProduct];
  //     this.setState({ arrBasket: updatedBasket });
  //   }
  // };

  // addToBasket = () => {
  //   !this.state.arrBasket.includes(this.state.selectedProduct) &&
  //     this.setState({
  //       arrBasket: [
  //         ...this.state.arrBasket,
  //         this.state.selectedProduct,
  //       ],
  //     });

  //   this.closeModal();
  // };
