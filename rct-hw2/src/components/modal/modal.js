import { Component } from "react";
import "./modal.scss";
import PropTypes from "prop-types";

class Modal extends Component {
  render() {
    const { header, closeButton, text, actions, onClick } = this.props;
    return (
      <div className="modal">
        <div className="modal__background" onClick={onClick}></div>
        <div className="modal__main">
          <div className="modal__header">
            <h2 className="modal__title">{header}</h2>
            {closeButton && (
              <div className="modal__close-button" onClick={onClick}></div>
            )}
          </div>
          <div className="modal__text">{text}</div>
          <div className="modal__actions">{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  onClick: PropTypes.func,
}

Modal.defaultProps = {
  header: "Do you want to add to basket this item?",
  text: "This item will be in your basket and you can continue your shopping.",
};

export default Modal;
