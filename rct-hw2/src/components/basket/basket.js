import { Component } from "react";
import "./backet.scss";
import PropTypes from "prop-types";
import bascketlogo from './img/add-icon.svg';

class Basket extends Component {

  render() {
    return (
      <a href="/#" className="header__basket">
        <img src={bascketlogo} alt="img" />
        {this.props.arrBasket.length}
      </a>
    );
  }
}

Basket.propTypes = {
  arrBasket: PropTypes.array,
};

export default Basket;



