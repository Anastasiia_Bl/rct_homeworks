import { Component } from "react";
import "../productList/productList.scss";
import ProductCard from "../productCard/productCard";
import PropTypes from "prop-types";

class ProductList extends Component {

    render() {
        const {arrProducts, openModal, addToFavourites} = this.props;
        const productCards = arrProducts.map(item => <ProductCard key={item.id} product={item} openModal={openModal} addToFavourites={addToFavourites}/>);
        return (
            <ul className="products__list">
                {productCards}
            </ul>
        );
    }
}

ProductList.propTypes = {
    arrProducts: PropTypes.array,
    openModal: PropTypes.func,
    addToFavourites: PropTypes.func,
}

export default ProductList;