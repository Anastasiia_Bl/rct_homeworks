import { Component } from "react";
import "./button.scss";
import PropTypes from 'prop-types';

class Button extends Component {

  render() {
    const { backgroundColor, text, onClick } = this.props;
    return (
      <button className="button__open-modal" style={{ backgroundColor }} onClick={onClick}>
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button;
