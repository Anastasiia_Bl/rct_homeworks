import { Component } from "react";
import "./favouriteBtn.scss";
import star from "./star.svg";
import starFav from "./starFavourite.svg";
import PropTypes from "prop-types";

// кнопка (зірочка) додати в обране
export class FavouriteBtn extends Component {
  constructor() {
    super();
    this.state = {
      onClick: false,
    };
  }
  componentDidMount = () => {
    JSON.parse(localStorage.getItem("arrFavourites")).forEach((element) => {
      if (
        JSON.stringify(element) === JSON.stringify(this.props.favouriteProduct)
      ) {
        this.setState({
          onClick: !this.state.onClick,
        });
      }
    });
  };
  render() {
    const { addToFavourites, favouriteProduct } = this.props;
    return (
      <a
        className="product__favourite-btn"
        href="/#"
        onClick={() => {
          addToFavourites(favouriteProduct);
          this.setState({
            onClick: !this.state.onClick,
          });
        }}
      >
        {this.state.onClick ? (
          <img src={starFav} alt="img" />
        ) : (
          <img src={star} alt="img" />
        )}
      </a>
    );
  }
}

FavouriteBtn.propTypes = {
  addToFavourites: PropTypes.func,
  favouriteProduct: PropTypes.object,
};
