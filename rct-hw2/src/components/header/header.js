import { Component } from "react";
import './header.scss';
import Basket from "../basket/basket";
import Favourites from "../favourites/favourites";
import PropTypes from "prop-types";

class Header extends Component {

    render() {
        return (
            <header className="header">
              <h2 className="header__title">Online coffee shop</h2>
              <div className="header__icons">
                <Basket arrBasket={this.props.arrBasket} />
                <Favourites arrFavourites={this.props.arrFavourites} />
              </div>
            </header>
        );
      }
}

Header.propTypes = {
  arrBasket: PropTypes.array,
  arrFavourites: PropTypes.array,
};

export default Header;