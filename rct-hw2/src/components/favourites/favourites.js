import { Component } from "react";
import "./favourites.scss";
import PropTypes from "prop-types";
import star from './img/star.svg'

class Favourites extends Component {

  render() {
    return (
      <a className="header__favourites" href="/#">
        <img src={star} alt="img" />
        {this.props.arrFavourites.length}
      </a>
    );
  }
}

Favourites.propTypes = {
  arrFavourites: PropTypes.array,
};

export default Favourites;
